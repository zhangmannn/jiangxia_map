define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'construction/index' + location.search,
                    add_url: 'construction/add',
                    edit_url: 'construction/edit',
                    del_url: 'construction/del',
                    multi_url: 'construction/multi',
                    import_url: 'construction/import',
                    table: 'construction',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                fixedColumns: true,
                fixedRightNumber: 1,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'type.nickname', title: __('Type_id')},
                        {field: 'region.nickname', title: __('Region_id')},
                        {field: 'number', title: __('Number')},
                        {field: 'nickname', title: __('Nickname'), operate: 'LIKE', table: table, class: 'autocontent', formatter: Table.api.formatter.content},
                        {field: 'area', title: __('Area'), operate:'BETWEEN'},
                        {field: 'building_area', title: __('Building_area'), operate:'BETWEEN'},
                        {field: 'stock_space', title: __('Stock_space'), operate:'BETWEEN'},
                        {field: 'main_industry', title: __('Main_industry'), operate: 'LIKE', table: table, class: 'autocontent', formatter: Table.api.formatter.content},
                        {field: 'fire_fighting_level', title: __('Fire_fighting_level'), operate: 'LIKE', table: table, class: 'autocontent', formatter: Table.api.formatter.content},
                        {field: 'monthly_rent', title: __('Monthly_rent'), operate: 'LIKE', table: table, class: 'autocontent', formatter: Table.api.formatter.content},
                        {field: 'contacts', title: __('Contacts'), operate: 'LIKE', table: table, class: 'autocontent', formatter: Table.api.formatter.content},
                        {field: 'contacts_phone', title: __('Contacts_phone'), operate: 'LIKE', table: table, class: 'autocontent', formatter: Table.api.formatter.content},
                        {field: 'address', title: __('Address'), operate: 'LIKE', table: table, class: 'autocontent', formatter: Table.api.formatter.content},
                        {field: 'coordinate', title: __('Coordinate'), operate: 'LIKE', table: table, class: 'autocontent', formatter: Table.api.formatter.content},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.append_data_params();
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.append_data_params();
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            append_data_params:function (){
                $(".industry").data("params", function(){
                    return {"custom[is_leaf]":true};
                });
            }
        }
    };
    return Controller;
});
