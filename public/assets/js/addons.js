define([], function () {
    require([], function () {
    //绑定data-toggle=addresspicker属性点击事件

    $(document).on('click', "[data-toggle='addresspicker']", function () {
        var that = this;
        var callback = $(that).data('callback');
        var input_id = $(that).data("input-id") ? $(that).data("input-id") : "";
        var lat_id = $(that).data("lat-id") ? $(that).data("lat-id") : "";
        var lng_id = $(that).data("lng-id") ? $(that).data("lng-id") : "";
        var lat = lat_id ? $("#" + lat_id).val() : '';
        var lng = lng_id ? $("#" + lng_id).val() : '';
        var url = "/addons/address/index/select";
        url += (lat && lng) ? '?lat=' + lat + '&lng=' + lng : '';
        Fast.api.open(url, '位置选择', {
            callback: function (res) {
                input_id && $("#" + input_id).val(res.address).trigger("change");
                lat_id && $("#" + lat_id).val(res.lat).trigger("change");
                lng_id && $("#" + lng_id).val(res.lng).trigger("change");
                try {
                    //执行回调函数
                    if (typeof callback === 'function') {
                        callback.call(that, res);
                    }
                } catch (e) {

                }
            }
        });
    });
});

require.config({
    paths: {
        'summernote': '../addons/summernote/lang/summernote-zh-CN.min'
    },
    shim: {
        'summernote': ['../addons/summernote/js/summernote.min', 'css!../addons/summernote/css/summernote.min.css'],
    }
});
require(['form', 'upload'], function (Form, Upload) {
    var _bindevent = Form.events.bindevent;
    Form.events.bindevent = function (form) {
        _bindevent.apply(this, [form]);
        try {
            //绑定summernote事件
            if ($(Config.summernote.classname || '.editor', form).length > 0) {
                var selectUrl = typeof Config !== 'undefined' && Config.modulename === 'index' ? 'user/attachment' : 'general/attachment/select';
                require(['summernote'], function () {
                    var imageButton = function (context) {
                        var ui = $.summernote.ui;
                        var button = ui.button({
                            contents: '<i class="fa fa-file-image-o"/>',
                            tooltip: __('Choose'),
                            click: function () {
                                parent.Fast.api.open(selectUrl + "?element_id=&multiple=true&mimetype=image/", __('Choose'), {
                                    callback: function (data) {
                                        var urlArr = data.url.split(/\,/);
                                        $.each(urlArr, function () {
                                            var url = Fast.api.cdnurl(this, true);
                                            context.invoke('editor.insertImage', url);
                                        });
                                    }
                                });
                                return false;
                            }
                        });
                        return button.render();
                    };
                    var attachmentButton = function (context) {
                        var ui = $.summernote.ui;
                        var button = ui.button({
                            contents: '<i class="fa fa-file"/>',
                            tooltip: __('Choose'),
                            click: function () {
                                parent.Fast.api.open(selectUrl + "?element_id=&multiple=true&mimetype=*", __('Choose'), {
                                    callback: function (data) {
                                        var urlArr = data.url.split(/\,/);
                                        $.each(urlArr, function () {
                                            var url = Fast.api.cdnurl(this, true);
                                            var node = $("<a href='" + url + "'>" + url + "</a>");
                                            context.invoke('insertNode', node[0]);
                                        });
                                    }
                                });
                                return false;
                            }
                        });
                        return button.render();
                    };

                    $(Config.summernote.classname || '.editor', form).each(function () {
                        $(this).summernote($.extend(true, {}, {
                            // height: 250,
                            minHeight: 250,
                            lang: 'zh-CN',
                            fontNames: [
                                'Arial', 'Arial Black', 'Serif', 'Sans', 'Courier',
                                'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande',
                                "Open Sans", "Hiragino Sans GB", "Microsoft YaHei",
                                '微软雅黑', '宋体', '黑体', '仿宋', '楷体', '幼圆',
                            ],
                            fontNamesIgnoreCheck: [
                                "Open Sans", "Microsoft YaHei",
                                '微软雅黑', '宋体', '黑体', '仿宋', '楷体', '幼圆'
                            ],
                            toolbar: [
                                ['style', ['style', 'undo', 'redo']],
                                ['font', ['bold', 'underline', 'strikethrough', 'clear']],
                                ['fontname', ['color', 'fontname', 'fontsize']],
                                ['para', ['ul', 'ol', 'paragraph', 'height']],
                                ['table', ['table', 'hr']],
                                ['insert', ['link', 'picture', 'video']],
                                ['select', ['image', 'attachment']],
                                ['view', ['fullscreen', 'codeview', 'help']],
                            ],
                            buttons: {
                                image: imageButton,
                                attachment: attachmentButton,
                            },
                            dialogsInBody: true,
                            followingToolbar: false,
                            callbacks: {
                                onChange: function (contents) {
                                    $(this).val(contents);
                                    $(this).trigger('change');
                                },
                                onInit: function () {
                                },
                                onImageUpload: function (files) {
                                    var that = this;
                                    //依次上传图片
                                    for (var i = 0; i < files.length; i++) {
                                        Upload.api.send(files[i], function (data) {
                                            var url = Fast.api.cdnurl(data.url, true);
                                            $(that).summernote("insertImage", url, 'filename');
                                        });
                                    }
                                }
                            }
                        }, $(this).data("summernote-options") || {}));
                    });
                });
            }
        } catch (e) {

        }

    };
});



require.config({
    paths: {
        'leaflet': '../addons/wleaflet/leaflet/leaflet',
        'proj4':'/assets/addons/wleaflet/leaflet/plugins/proj4/proj4',
        'proj4leaflet_min':'/assets/addons/wleaflet/leaflet/plugins/proj4/proj4leaflet.min',
        'leaflet_ChineseTmsProviders':'/assets/addons/wleaflet/leaflet/leaflet.ChineseTmsProviders',
        'mapCorrection':'/assets/addons/wleaflet/leaflet/leaflet.mapCorrection',
        'leaflet_geoman_min':'/assets/addons/wleaflet/leaflet/plugins/leaflet-geoman-free/leaflet-geoman.min',
        'leaflet_common': '../addons/wleaflet/leaflet/leaflet_common'
    },
    shim: {
        'leaflet':{
            deps:[
                'css!../addons/wleaflet/leaflet/leaflet.css',
            ],
            exports:'L'
        },
        'leaflet_common': {
            deps:[
                'css!../addons/wleaflet/leaflet/plugins/leaflet-geoman-free/leaflet-geoman.css',
            ],
            exports: 'wLeaflet',
        },
    }
});

window.initWleafletFunc = function(callback)
{
    try{
        require(['proj4leaflet_min'], function () {
            require(['leaflet_ChineseTmsProviders'],function () {
                require(['mapCorrection','leaflet_geoman_min'],function () {
                    require(['leaflet_common'],function () {
                        if(typeof callback == 'function')
                        {
                            callback(wLeaflet,L)
                        }else{
                            console.log('缺少callback参数');
                        }
                    })
                })
            })
        })
    }catch (e) {
        if(typeof callback == 'function')
        {
            callback({})
        }else{
            console.log('初始化地图插件失败')
        }
    }
}

require([], function () {

    //绑定data-toggle=addresspicker属性点击事件
    $(document).on('click', "[data-toggle='wleaflet-select']", function () {
        var that = this;
        var callback = $(that).data('callback');
        var input_id = $(that).data("input-id") ? $(that).data("input-id") : "";
        var sel_type = $(that).data('type')? $(that).data("type") : "address"; //默认选择地理位置
        var url = "/index.php/addons/wleaflet/index/select?sel_type="+sel_type;
        switch (sel_type) {
            case 'address':
                //选择地理位置
                var lat_id = $(that).data("lat-id") ? $(that).data("lat-id") : "";
                var lng_id = $(that).data("lng-id") ? $(that).data("lng-id") : "";
                var address = input_id ? $("#" + input_id).val() : '';
                var lat = lat_id ? $("#" + lat_id).val() : '';
                var lng = lng_id ? $("#" + lng_id).val() : '';
                if(lat && lng)
                {
                    url = url +'&lat=' + lat + '&lng=' + lng;
                }
                if(address)
                {
                    url = url + '&address='+address;
                }
                break;
            case 'line':
                var limit_line_count = $(that).data("limit-line-count") ? $(that).data("limit-line-count") : 0;
                var latlng = input_id ? $("#" + input_id).val() : '';
                url = url +'&latlng='+latlng+'&limit_line_count='+limit_line_count;
                break;
            case 'marker':
            case 'polygon':
                //选择marker点
                var latlng = input_id ? encodeURIComponent( $("#" + input_id).val()) : '';
                url = url +'&latlng='+latlng;
                break;
            default:
                url = '';
                break;
        }
        if(url)
        {
            Fast.api.open(url, '地图操作', {
                callback: function (res) {

                    switch (sel_type) {
                        case 'address':
                            input_id && $("#" + input_id).val(res.address);
                            lat_id && $("#" + lat_id).val(res.lat);
                            lng_id && $("#" + lng_id).val(res.lng);
                            break;
                        case 'marker':
                        case 'line':
                        case 'polygon':
                            input_id && $("#" + input_id).val(res.latlng);
                            break;
                        default:
                            break;
                    }
                    try {
                        //执行回调函数
                        if (typeof callback === 'function') {
                            callback.call(that, res);
                        }
                    } catch (e) {

                    }
                }
            });
        }

    });
});
});