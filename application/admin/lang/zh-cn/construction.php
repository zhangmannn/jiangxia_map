<?php

return [
    'Type_id'             => '类型id',
    'Region_id'           => '区域id',
    'Number'              => '序号',
    'Nickname'            => '园区名称',
    'Area'                => '占地面积(亩)',
    'Building_area'       => '建筑面积',
    'Stock_space'         => '存量空间',
    'Main_industry'       => '主要产业',
    'Fire_fighting_level' => '消防等级',
    'Specifications'      => '技术指标',
    'Monthly_rent'        => '月租金',
    'Contacts'            => '联系人',
    'Contacts_phone'      => '联系方式',
    'Address'             => '园区地址',
    'Supporting'          => '周边配套',
    'Coordinate'          => '坐标',
    'Coordinate_region'   => '范围坐标'
];
