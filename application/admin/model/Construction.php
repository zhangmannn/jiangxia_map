<?php

namespace app\admin\model;

use think\Model;


class Construction extends Model
{





    // 表名
    protected $name = 'construction';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];

    public function type(){
        return $this->belongsTo(Type::class,'type_id')->field('id,nickname');
    }

    public function region(){
        return $this->belongsTo(Region::class,'region_id')->field('id,nickname');
    }










}
