<?php

return [
    'autoload' => false,
    'hooks' => [
        'config_init' => [
            'summernote',
            'wleaflet',
        ],
    ],
    'route' => [],
    'priority' => [],
    'domain' => '',
];
