<?php
namespace app\api\controller;

use app\common\controller\Api;
use think\Request;

class File extends Api{

    protected $noNeedLogin = ['*'];


    public function __construct(Request $request = null)
    {
        parent::__construct($request);


    }


    public function getFile(){
        $category = $this->request->get('category');
        if(empty($category)){
            $this->error('参数必填');
        }
        $category_arr = explode(',',$category);
        $where = [
            'category'=>['in',$category_arr]
        ];
        //->whereNotNull('your_column')
        $data = db('attachment')->where($where)->field('id,url,filename,mimetype,sha1,category')->select();
        $this->success('成功',$data);

    }
}
