<?php

namespace app\api\controller;

use app\common\controller\Api;
use think\Model;

class Industry extends Api
{

    protected $noNeedLogin = ["*"];

    protected Model $model;

    protected function _initialize()
    {
        parent::_initialize(); // TODO: Change the autogenerated stub
        $this->model = new \app\admin\model\Industry();
    }

    /**
     * @return void
     */
    public function getIndustry(){
        $data = $this->model->field('id,name,avatar')->where('pid',0)->select();
        $this->success('成功',$data);
    }


}
